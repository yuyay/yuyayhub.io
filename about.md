---
layout: page
permalink: /about/
---

# About
{: style="color: teal"}


## Education

- Ph.D., Engineering, Nara Institute of Science and Technology (NAIST), Sep. 2015.
- M.S., Engineering, Nara Institute of Science and Technology (NAIST), Mar. 2013.
- B.S., Management and Information, University of Shizuoka, Mar. 2011.

## Work Experience

- **October 2015 -- Current**
  - Chief Research Scientist, Software Technology and Artificial Intelligence Research Laboratory (STAIR Lab), Chiba Institute of Technology.

- **April 2013 -- September 2015**
  - JSPS Research Fellowship (DC1)


## Academic Activities

- PDPTA2016 (reviewer), NIPS2016 (reviewer), AIRS2016 (PC member), JSAI2017 (PC member), TJSAI (reviewer), IEICE TRANSACTIONS on Information and Systems (reviewer)

- [STAIR Lab AI Seminar/Symposium](https://stair.connpass.com) (organizer) 


## Awards

- NAIST Best Student Award, Mar. 2016.
- JSAI Annal Conference Award 2014, May 2014.
- Student Incentive Award / Cyber Agent Award, WebDB Forum 2012, Nov. 2012.
- Student Incentive Award, The 50th IPSJ Annual Conference, Mar. 2012.

