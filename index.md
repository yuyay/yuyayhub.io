---
layout: page
permalink: /
---

Currently, I'm a chief research scientist at
[Software Technology and Artificial Intelligence Research Laboratory (STAIR Lab)](https://stair.center),
[Chiba Institute of Technology](http://www.it-chiba.ac.jp),
Japan.

現在、[千葉工業大学](http://www.it-chiba.ac.jp) [人工知能・ソフトウェア技術研究センター](https://stair.center)の主任研究員として働いています。


## News

- April 2, 2017: Our paper entitled ``STAIR Captions: Constructing a Large-Scale Japanese Image Caption Dataset'' has been accepted as a short paper in ACL2017. 
  - The preprint version is available at arXiv: [https://arxiv.org/abs/1705.00823](https://arxiv.org/abs/1705.00823)
  - The dataset can be downloaded: [[Project page](http://captions.stair.center)].
- March 15, 2017: We have presented our paper about Japanese image caption dataset in NLP2017. [[Project page](http://captions.stair.center)]


## Contact

Yuya Yoshikawa (吉川 友也), yoshikawa--at--stair.center


 
