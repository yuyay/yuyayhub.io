---
layout: page
permalink: /publications/
---

[[Japanese Publications]](./ja)

# Publications
{: style="color: teal"}

## Refereed International Conferences

1. Yuya Yoshikawa, Yutaro Shigeto, Akikazu Takeuchi, "STAIR Captions: Constructing a Large-Scale Japanese Image Caption Dataset," Annual Meeting of the Association for Computational Linguistics (ACL2017 Short), Vancouver, Canada, July 2017. (to appear) [[arXiv](https://arxiv.org/abs/1705.00823)]
1. Yuya Yoshikawa, Tomoharu Iwata, Hiroshi Sawada, Takeshi Yamada, "Cross-Domain Matching for Bag-of-Words Data via Kernel Embeddings of Latent Distributions," Advances in Neural Information Processing Systems (NIPS2015), Montreal, Canada, Dec. 2015. [[paper](https://papers.nips.cc/paper/5959-cross-domain-matching-for-bag-of-words-data-via-kernel-embeddings-of-latent-distributions)]
1. Yuya Yoshikawa, Tomoharu Iwata, Hiroshi Sawada, "Non-linear Regression for Bag-of-Words Data via Gaussian Process Latent Variable Set Model," The 28th AAAI Conference on Artificial Intelligence (AAAI2015), Austin, Texas, USA, Jan. 2015. [[paper](https://www.aaai.org/ocs/index.php/AAAI/AAAI15/paper/view/9796)]
1. Yuya Yoshikawa, Tomoharu Iwata, Hiroshi Sawada, "Latent Support Measure Machines for Bag-of-Words Data Classification," Advances in Neural Information Processing Systems (NIPS2014), Montreal, Canada, Dec. 2014. [[paper](https://papers.nips.cc/paper/5480-latent-support-measure-machines-for-bag-of-words-data-classification)]
1. Yuya Yoshikawa, Tomoharu Iwata, Hiroshi Sawada, "Latent Feature Independent Cascade Model for Social Propagation," International Conference on Parallel and Distributed Processing Techniques and Application (PDPTA2013), Las Vegas, USA, July 2013. [[paper](http://worldcomp-proceedings.com/proc/p2013/PDP2170.pdf)]
1. Yuya Yoshikawa, Kazumi Saito, Hiroshi Motoda, Kouzou Ohara, and Masahiro Kimura, “Acquiring Expected Influence Curve from Single Diffusion Sequence,” Proc. of the 2010 Pacific Rim Knowledge Acquisition Workshop (PKAW2010), pp.273–287, Daegu, Korea,  Sep. 2010. [[paper](/papers/yuyay-pkaw2010.pdf)]


## Non-Refereed International Conferences

1. Yuya Yoshikawa, "Topic-dependent Information Diffusion Model on Social Networks," Machine Learning Summer School (MLSS2012), Kyoto, Sep. 2012. [[poster](/papers/yuyay-mlss2012.pdf)]


## Preprints

1. Yuya Yoshikawa, Tomoharu Iwata, Hiroshi Sawada, "Collaboration on Social Media: Analyzing Successful Projects on Social Coding," Arxiv:1408.6012, Aug. 2014. [[arXiv](https://arxiv.org/abs/1408.6012)]

